const random = require('random');

// Функции по генерации и бинарному поиску
class ArraySearch {
  constructor() {
    this.arrayLength = 100;
    this.maxNum = 100;
  }

  // Метод для генерации массива случайных чисел и его сортировки
  ArrayGen() {
    const arr = Array.from({ length: this.arrayLength }, () =>
      random.int(1, this.maxNum) // Генерация чисел
    ).sort((a, b) => a - b);
    console.log("Generated array:", arr);
    return arr;
  }

  // Метод для выполнения бинарного поиска значения в массиве
  ArraySearch(val, arr) {
    let start = 0;
    let end = arr.length - 1;
    while (start <= end) {
      let mid = Math.floor((start + end) / 2);

      if (arr[mid] === val) {
        return mid;
      }
      if (val < arr[mid]) {
        end = mid - 1;
      } else {
        start = mid + 1;
      }
    }
    return -1;
  }
}

// Экспортируем класс ArraySearch для использования в других модулях
module.exports = ArraySearch;
