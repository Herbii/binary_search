const ArraySearch = require('./search');

// Получение искомого значения из аргументов командной строки
const value = parseInt(process.env.TARGET_VALUE, 10); // Преобразуем значение в целое число
if (isNaN(value)) {
  console.error('Usage: TARGET_VALUE=<value>');
  process.exit(1);
}

const arraySearch = new ArraySearch();
const arr = arraySearch.ArrayGen(); // Генерируем массив
const result = arraySearch.ArraySearch(value, arr);

if (result !== -1) {
  console.log(`Value ${value} found at index ${result}.`);
} else {
  console.log(`Value ${value} not found.`);
}
